const request = require('request-promise');

const apiUrl = 'http://ec2-35-159-11-170.eu-central-1.compute.amazonaws.com/casino/';

const mtGenerator = require('./mt19937').generator;

const Random = require("random-js");

const getResponseOfGame = (mode, number, bet) => {
    const options = {
        // uri: `${apiUrl}play${mode}?id=yaros8&bet=${bet}&number=${number}`,
        uri: `https://us-central1-oleksmir-test.cloudfunctions.net/mt`,
        json: true 
    };
    return request(options);
}

const findConstants = (resultNumbers) => {
    const a = ((resultNumbers[1] - resultNumbers[0]) % Math.pow(2,32));
    const b = (resultNumbers[2] - resultNumbers[1]);
    console.log(a, b, 2/3);
};

const div = (val, by) => {
    return (val - val % by) / by;
}

const loop = (promise, fn, catchFn) => {
    return promise.then(fn).catch(catchFn).then(function (wrapper) {
      return !wrapper.done ? loop(wrapper.promise, fn, catchFn) : 'done';
    });
  }

const lcg = (bet) => {
    getResponseOfGame('Lcg', 1, bet).then( data => {
        let balance = data.account.money;
        let nextNumber = (1664525 * data.realNumber + 1013904223) % Math.pow(2,32);
        console.log(nextNumber, balance);
        loop(getResponseOfGame('Lcg', nextNumber, bet), (data) => {
            balance = data.account.money;
            nextNumber = (1664525 * data.realNumber + 1013904223) % Math.pow(2,32);
            console.log('Current balance: ',balance);
            return {done: balance > 1000000, promise: getResponseOfGame('Lcg', nextNumber, bet)}; 
        }, (error) => {
            nextNumber = (1664525 * nextNumber + 1013904223) % Math.pow(2,32); 
            return { done: false, promise: getResponseOfGame('Lcg', nextNumber, bet)}
        });
    })
}
const mt = (bet) => {
    getResponseOfGame('Mt', 1, 1).then( res => {
        console.log(res);
        let lastNumber = res.realNumber;
        const mt19937Generator = new mtGenerator();
        let seedResolved = false;
        const seed = Number.parseInt(Date.now() / 1000);
        for (let offset = 0; offset <= 1000; ++offset) {
            const mt19937Generator = new mtGenerator();
            mt19937Generator.init_genrand(seed - offset);
            let rand = mt19937Generator.genrand_int32();
            console.log(rand, lastNumber);
			if (rand === lastNumber) {
				seedResolved = true;
				break;
			}
        }
        console.log(seedResolved);
        for(let i = 0; i < 10; i++) {
            console.log(mt19937Generator.genrand_int32());
        }
    })
}

const mtHard = (bet) => {
    const unBitshiftLeftXor = (val, shift) => {
        let i = 0;
        let result = 0;
        while (i * shift < 32) {
            let partMask = (-1 << (32 - shift)) >>> (shift * i);
            let part = val & partMask;
            val ^= part >>> shift;
            result |= part;
            i++;
        }
        return result;
    }
     
    const unBitshiftLeftXor = (val, shift, mask) => {
        let i = 0;
        let result = 0;
        while (i * shift < 32) {
            let partMask = (-1 >>> (32 - shift)) << (shift * i);
            let part = val & partMask;
            val ^= (part << shift) & mask;
            result |= part;
            i++;
        }
        return result;
    }
    
    let state = new Array(624);

    const hack = () => {
        let counter = 0;
        loop(getResponseOfGame('BetterMt', 1, bet), (data) => {
            balance = data.account.money;
            number =  data.realNumber;
            number = unBitshiftLeftXor(number, 18);
            number = unBitshiftLeftXor(number, 15, 0xEFC60000);
            number = unBitshiftLeftXor(number, 7, 0x9D2C5680);
            number = unBitshiftLeftXor(number, 11);
            state[i] = number;
            console.log('Current balance: ',balance);
            counter++;
            return {done: counter >= 624, promise: getResponseOfGame('BetterMt', 1, bet)}; 
        });
    }
    
    hack();
    const generator = new mtGenerator();
    generator.init_by_array(state);
    
    let money = 0;
    loop(getResponseOfGame('BetterMt', nextNumber, bet), (data) => {
        balance = data.account.money;
        nextNumber = generator.genrand_int32();
        console.log('Current balance: ',balance);
        return {done: balance > 1000000, promise: getResponseOfGame('BetterMt', nextNumber, bet)}; 
    });
}

const main = () => {
    // lcg(2);
    // const mt19937Generator = new mtGenerator();
    // console.log(mt19937Generator.random());
    mt(1);
};

main();


